import firebase from 'firebase/app';
import firestore from 'firebase/firestore';

  var config = {
    apiKey: "AIzaSyDKXjFpxVnsqXbFlMq45PxRri5OxiI5SB8",
    authDomain: "vuejs-fire-base.firebaseapp.com",
    databaseURL: "https://vuejs-fire-base.firebaseio.com",
    projectId: "vuejs-fire-base",
    storageBucket: "vuejs-fire-base.appspot.com",
    messagingSenderId: "919135359744"
  };
  const firebaseApp = firebase.initializeApp(config);
  firebaseApp.firestore();

  export default firebaseApp.firestore();